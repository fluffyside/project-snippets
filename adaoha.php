<!DOCTYPE html>
<html lang="en">
<head>
	<link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/adaoha.css">
</head>
<body>
	<div class="wrapper">
		<div class="inner-wrapper">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<defs>
				<pattern id="p1" patternUnits="userSpaceOnUse" width="200" height="200">
					<image href="http://fluffyside.uk/assets/pattern.jpg" width="300" height="300" />
				</pattern>
				</defs>
				<text id="adaoha" x="50%" y="50%" text-anchor="middle" alignment-baseline="central">Adaoha</text>
			</svg>
			<svg>
				<text id="daughter" x="50%" y="50%" text-anchor="middle" alignment-baseline="central">Daughter of the people</text>
			</svg>
		</div>
</div>
</body>
</html>
