<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />
    <?php require_once 'function-convert-to-icons.php' ?>

    <style>
        /* ===== optional styling =====*/
        body, input {
            font-family: "Droid Serif", Serif;
            font-size: 16px;
        }
        .wrapper {
            max-width: 700px;
            margin: 50px auto;
            text-align: center;
        }
        #scores-container {
            max-width: 800px;
            margin: 10px auto;
            text-align: center;
        }
        .score {
            height: 60px;
            width: 60px;
            font-size: 20px;
            padding: 2px;
        }
        .stars-container {
            max-width: 600px;
            height: 80px;
            margin: 40px auto;
            display: block;
            background-color: #fff;
            position: relative;
            text-align: center;
            font-size: 20px;
        }
        li.stars {
            display: inline;
            list-style: none;
            color: #fc0;
            font-size: 40px;
        }
    </style>
</head>
<body>


	<div class="wrapper">
		<h1>Javascript Stars Rating</h1>
      <form>
        <input type="number" name="max" class="score" autocomplete="off" min="0" max="5" step="1" value="0" id="StarRating" required />
      </form>

	</div>

    <div class="wrapper">

      <ul>
      <li id="stars-container" class="stars">
        <i class="fa fa-star-o"></i>
        <i class="fa fa-star-o"></i>
        <i class="fa fa-star-o"></i>
        <i class="fa fa-star-o"></i>
        <i class="fa fa-star-o"></i>
       </li>
      </ul>

    </div>

<script type="text/javascript">
$("#StarRating").change(function() {
  var Stars = this.defaultValue < this.value;
  this.defaultValue = this.value;
     if (Stars) {  
       $("#stars-container > i:nth-of-type(-n+" + (this.defaultValue) + ")").removeClass("fa-star-o").addClass("fa-star");
            }
        else 

        $("#stars-container > i:nth-of-type(n+" + ((this.defaultValue * 1 ) + 1) + ")").removeClass("fa-star").addClass("fa-star-o");
});

</script>
</body>
</html>