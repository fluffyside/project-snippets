<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="change-scores.css"  />
</head>
<body>
	<div class="wrapper">
		<h1>Change Scores</h1>
		<form>
		  <input type="number" name="T1" class="score" autocomplete="off" min="0" max="8" step="0.5" id="EnterScoreT1" required />
		  <input type="number" name="T2" class="score" autocomplete="off" min="0" max="8" step="0.5" id="EnterScoreT2" required />
		</form>

<div class="container">
  <div class="left-container" id="Team1">
    <div class="points" id="">0.5</div>
    <div class="points" id="">1</div>
    <div class="points" id="">1.5</div>
    <div class="points" id="">2</div>
    <div class="points" id="">2.5</div>
    <div class="points" id="">3</div>
    <div class="points" id="">3.5</div>
    <div class="points" id="">4</div>
    <div class="points" id="">4.5</div>
    <div class="points" id="">5</div>
    <div class="points" id="">5.5</div>
    <div class="points" id="">6</div>
    <div class="points" id="">6.5</div>
    <div class="points" id="">7</div>
    <div class="points" id="">7.5</div>
    <div class="points" id="">8</div>
    <div id="Team1Score" class="team1-score">0</div>
  </div>
  <div class="right-container" id="Team2">
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div class="points" id=""></div>
    <div id="Team2Score" class="team2-score">0</div>
  </div>
</div>

	</div>

<script type="text/javascript">


$("#EnterScoreT1").change(function() {
	    var Team1Score = this.defaultValue < this.value
    	this.defaultValue = this.value;
    	if (Team1Score) {        
    		$("#Team1 > div.points:nth-of-type(-n+" + (this.defaultValue * 2) + ")").addClass("t1");
        $("#Team1Score").text(this.defaultValue);
		    }
	    else 
    		$("#Team1 > div.points:nth-of-type(n+" + ((this.defaultValue * 2) + 1) + ")").removeClass("t1");
        $("#Team1Score").text(this.defaultValue);
});
//Team 2 //
$("#EnterScoreT2").change(function() {
  var MaxScore = 17;
	    var Team2Score = this.defaultValue < this.value
    	this.defaultValue = this.value;
    	if (Team2Score) {        
    		$("#Team2 > div.points:nth-of-type(n+ " + (MaxScore - (this.defaultValue * 2) ) + ")").addClass("t2");
        $("#Team2Score").text(this.defaultValue);
		    }
	    else      
    		$("#Team2 > div.points:nth-of-type(-n+ " + (MaxScore - (this.defaultValue * 2) -1 ) + ")").removeClass("t2");
        $("#Team2Score").text(this.defaultValue);
});



</script>
</body>
</html>