<?php
function stars($max_stars = 0, $user_rating = 0) {
    $whole_stars = floor($user_rating);
    $fraction = $user_rating - $whole_stars;
    if ($fraction > 0) {
        $half_stars = 1 ;
        }
    else {
        $half_stars = 0;
    }
    $no_stars = $max_stars - $whole_stars - $half_stars;
    $star = 0;
    $empty_star = 0;
        for ($star = 1; $star <= $user_rating; $star++) {
        echo "<li><i class=\"fa fa-star\"></i></li>";
        } 

        if ($half_stars > 0) {
                echo "<li><i class=\"fa fa-star-half-o\"></i></li>";
            }
        while ($empty_star < $no_stars) {
                echo "<li><i class=\"fa fa-star-o\"></i></li>";
                $empty_star++;
            }
    }
?>