<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
	<style>
	 		/* ===== optional styling =====*/
			body, input {
				font-family: "Droid Serif", Serif;
				font-size: 16px;
			}
			.wrapper {
				max-width: 700px;
				margin: 50px auto;
				text-align: center;
			}
			input {
				margin-bottom: 50px;
			}
			.red {
				color: red;
			}
	</style>
</head>
<body>
	<div class="wrapper">
		<h1>Display Scores</h1>

		<form>
			<input id="EnterScoreT1" type="number" size="50" maxlength="140" />
			<div><h3 id="Team1Score"></h3></div>

		</form>

	</div>

<script type="text/javascript">

	$("#EnterScoreT1").change(function() {
	    var TheScore = this.defaultValue < this.value
    	this.defaultValue = this.value;
    	if (TheScore) { 
    		$("#Team1Score").text(this.defaultValue).addClass("red");
		    }
	    else 
    		$("#Team1Score").text(this.defaultValue).removeClass("red");
	});


</script>
</body>
</html>