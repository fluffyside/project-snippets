<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
	<style>
	 		/* ===== optional styling =====*/
 			html { 
			  background: url(img/balloons.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
			}
			body, input {
				font-family: "Droid Serif", Serif;
				font-size: 16px;
				color: #fff;
			}
			.wrapper {
				max-width: 700px;
				margin: 50px auto;
				text-align: center;
			}
			h1 {
				margin-top: 45vh;
				text-shadow: 1px 1px 3px #000;

			}
	</style>
</head>
<body>
	<div class="wrapper">
		<h1>Full screen background</h1>

	</div>
</body>
</html>