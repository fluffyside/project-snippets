<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
	<style>
	 		/* ===== optional styling =====*/
			body, input {
				font-family: "Droid Serif", Serif;
				font-size: 16px;
			}
			.wrapper {
				max-width: 700px;
				margin: 50px auto;
				text-align: center;
			}
			input {
				margin-bottom: 50px;
			}
	</style>
</head>
<body>
	<div class="wrapper">
		<h1>Character Count</h1>

		<form>
			<input id="YourMessage" type="text" size="50" maxlength="140" />
			<div><h3 id="FeedbackContainer">Start typing your message in the form above.</h3></div>

		</form>

	</div>

<script type="text/javascript">
	var MaxMessageLength = 140;

	$("#YourMessage").keyup(function() {
		var MessageLength = $("#YourMessage").val().length;
		var LettersRemaining = MaxMessageLength - MessageLength;

		$("#FeedbackContainer").html("You have typed " + MessageLength + " letters, and have " + LettersRemaining + " letters remaining.");
	});

</script>
</body>
</html>