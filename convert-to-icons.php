<!DOCTYPE html>
<html lang="en">
<head>
	<title>jQuery character count</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />
    <?php require_once 'function-convert-to-icons.php' ?>

    <style>
            /* ===== optional styling =====*/
            body, input {
                font-family: "Droid Serif", Serif;
                font-size: 16px;
            }
            .wrapper {
                max-width: 700px;
                margin: 50px auto;
                text-align: center;
            }
            li {
                list-style: none;
                display: inline;
                padding: 0 3px;
                font-size: 2em;
                color: #fc0;
            }
            pre.code {
                background-color: #333;
                color: #fff;
                font-size: 0.8em;
                text-align: left;
            }
            pre.code:before {
                content: 'PHP code';
                display: block;
                background-color: #999;
                font-size: 2em;
                font-weight: bold;
            }
    </style>
</head>
<body>
    <?php

    if (!isset($_POST["SubmitStars"])) {
        $MaxStars = 0;
        $AwardedStars = 0;
    }
    else {
        $MaxStars = $_POST["MaxStars"];
        $AwardedStars = $_POST["AwardedStars"];
    }

    ?>

	<div class="wrapper">
		<h1>Display Stars</h1>
        <p><?php stars($MaxStars,$AwardedStars) ?></p>
        <form action="" method="post">
            Maximum Stars: <input name="MaxStars" type="number" class="score" autocomplete="off" min="0" max="10" step="1" required /><br><br>
            Stars awarded: <input name="AwardedStars" type="number" class="score" autocomplete="off" min="0" max="10" step="0.5" required />
            <input type="submit" name="SubmitStars">
        </form>
	</div>

    <div class="wrapper">
        <pre class="code">

    function stars($max_stars = 0, $user_rating = 0) {

        // seperate the wohle number form the fraction
        $whole_stars = floor($user_rating);

        // Determine whether there is a half point or not
        $fraction = $user_rating - $whole_stars;
        if ($fraction > 0) {
            $half_stars = 1 ;
            }
        else {
            $half_stars = 0;
        }
        
        // Calculate the empty stars
        $no_stars = $max_stars - $whole_stars - $half_stars;
        $star = 0;
        $empty_star = 0;
            for ($star = 1; $star <= $user_rating; $star++) {
            echo "<li><i class=\"fa fa-star\"></i></li>";
            } 

            if ($half_stars > 0) {
                    echo "<li><i class=\"fa fa-star-half-o\"></i></li>";
                }
            while ($empty_star < $no_stars) {
                    echo "<li><i class=\"fa fa-star-o\"></i></li>";
                    $empty_star++;
                }
        }
        </pre>
    </div>

<script type="text/javascript">


</script>
</body>
</html>